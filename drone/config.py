TELEMETRY_SERVER = "http://vps-ecdef220.vps.ovh.net:9998"

TELEMETRY_ENDPOINT = f"{TELEMETRY_SERVER}/telem"
DRONE_COMMAND_URL = f"{TELEMETRY_SERVER}/drone"
SEND_PHOTO_URL = f"{TELEMETRY_SERVER}/image"


ROBUR_ENDPOINT = "http://79.189.78.141:9999/predict"

# 0's mean no compression at all
CAMERA_PHOTO_SIZE_X = 1200
CAMERA_PHOTO_SIZE_Y = 800

# Possible valid compression values used in previous flights: 1200, 800
