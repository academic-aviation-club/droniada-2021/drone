import logging
import time

from dronekit import LocationGlobal, LocationGlobalRelative, VehicleMode, connect
from pymavlink import mavutil

logger = logging.getLogger(__name__)


class Mavlink:
    def __init__(
        self,
        connection_string: str,
        position_callback,
        gps_positon_callback,
        attitude_callback,
        battery_callback,
        rangefinder_callback,
    ):
        self.connection_string = connection_string
        self.vehicle = connect(self.connection_string, baud=57600, wait_ready=True)

        self.ned_location_callback = position_callback
        self.vehicle.add_attribute_listener(
            "location.local_frame", self.location_listener
        )

        self.gps_location_callback = gps_positon_callback
        self.vehicle.add_attribute_listener(
            "location.global_relative_frame", self.location_gps_listener
        )

        self.attitude_callback = attitude_callback
        self.vehicle.add_attribute_listener("attitude", self.attitude_callback)

        self.battery_callback = battery_callback
        self.vehicle.add_attribute_listener("battery", self.battery_callback)

        self.rangefinder_callback = rangefinder_callback
        self.vehicle.add_attribute_listener("rangefinder", self.rangefinder_callback)

    def arm_and_takeoff(self, aTargetAltitude):
        print("Basic pre-arm checks")
        # Don't let the user try to arm until autopilot is ready
        while not self.vehicle.is_armable:
            print(" Waiting for vehicle to initialise...")
            time.sleep(1)

        print("Arming motors")
        # Copter should arm in GUIDED mode
        self.vehicle.mode = VehicleMode("GUIDED")
        self.vehicle.armed = True

        while not self.vehicle.armed:
            print(" Waiting for arming...")
            time.sleep(1)

        print("Taking off!")
        self.vehicle.simple_takeoff(aTargetAltitude)  # Take off to target altitude

        # Wait until the vehicle reaches a safe height before processing the goto (otherwise the command
        #  after Vehicle.simple_takeoff will execute immediately).
        while True:
            print(" Altitude: ", self.vehicle.location.global_relative_frame.alt)
            if (
                self.vehicle.location.global_relative_frame.alt
                >= aTargetAltitude * 0.95
            ):  # Trigger just below target alt.
                print("Reached target altitude")
                break
            time.sleep(1)

    def fly_to_gps(self, lat, lng, alt):
        logger.info("flying to lat %f, lng %f, alt %f", lat, lng, alt)
        point = LocationGlobalRelative(lat, lng, alt)
        self.vehicle.simple_goto(point)

    def fly_to_ned(self, north, east, down):
        msg = self.vehicle.message_factory.set_position_target_local_ned_encode(
            0,  # time_boot_ms (not used)
            0,
            0,  # target system, target component
            mavutil.mavlink.MAV_FRAME_LOCAL_NED,  # frame
            0b0000111111111000,  # type_mask (only positions enabled)
            north,
            east,
            down,  # x, y, z positions (or North, East, Down in the MAV_FRAME_BODY_NED frame
            0,
            0,
            0,  # x, y, z velocity in m/s  (not used)
            0,
            0,
            0,  # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
            0,
            0,
        )  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
        # send command to vehicle
        self.vehicle.send_mavlink(msg)

    def change_flight_mode(self, mode):
        self.vehicle.mode = mode

    def location_listener(self, vehicle, _msg_name, location_local_frame):
        self.ned_location_callback(
            location_local_frame.north,
            location_local_frame.east,
            location_local_frame.down,
        )

    def location_gps_listener(self, vehicle, _msg_name, global_relative_frame):
        self.gps_location_callback(
            global_relative_frame.lat,
            global_relative_frame.lon,
            global_relative_frame.alt,
        )

    def attitude_listener(self, vehicle, _msg_name, attitude):
        self.attitude_callback(
            attitude.pitchh, attitude.roll, attitude.yaw, vehicle.heading
        )

    def rangefinder_listener(self, vehicle, _msg_name, rangefinder):
        self.rangefinder_callback(rangefinder.distance)


def example_callback(north, east, down):
    logger.debug(f"North: {north} East:{east} Down: {down}")


def example_gps_callback(lat, lon, alt):
    logger.debug(f"Lat: {lat} Lon:{lon} alt: {alt}")


def example_attitude_callback(pitch, roll, yaw):
    logger.debug(f"Pitch: {pitch} roll {roll} yaw {yaw}")


if __name__ == "__main__":
    pass
    # connection_string = "127.0.0.1:14550"

    # mavlink = Mavlink(
    #     connection_string,
    #     example_callback,
    #     example_gps_callback,
    #     example_attitude_callback,
    # )
    # mavlink.arm_and_takeoff(10)
    # mavlink.fly_to_gps(54, 18, 20)
    # logger.info("Connected")

    # arm_drone = Arming(vehicle)
    # arm_drone.arm_and_takeoff(10)

    # def location_callback(location):
    # print(location)

    # while True:
    #     time.sleep(1)
