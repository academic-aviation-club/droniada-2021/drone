import argparse

from drone.config import ROBUR_ENDPOINT, TELEMETRY_ENDPOINT


def get_standard_cli_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--connection-string",
        type=str,
        help="Connection string: UART or network-based",
        required=True,
    )
    parser.add_argument(
        "--telemetry-url", type=str, required=False, default=TELEMETRY_ENDPOINT
    )
    parser.add_argument("--robur-url", type=str, required=False, default=ROBUR_ENDPOINT)
    parser.add_argument(
        "--drone-name",
        type=str,
        required=True,
        help="Drone name to use",
    )

    return parser
