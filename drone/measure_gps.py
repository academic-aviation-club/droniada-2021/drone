import logging
import time

from drone.autonomy.droniada_drone_data_source import (
    TIME_BETWEEN_HOUSEKEEPING_TELEMETRY,
    DroniadaDataSource,
)
from drone.cli import get_standard_cli_parser
from drone.setup_logging import setup_logging

logger = logging.getLogger(__name__)

TIME_BETWEEN_MEASUREMENTS = 1


def avg(l: list):
    return sum(l) / len(l)


if __name__ == "__main__":
    setup_logging()

    args = get_standard_cli_parser().parse_args()
    droniada_data_source = DroniadaDataSource(args.connection_string)

    measurements_seconds = 0

    lat_list = []
    lon_list = []

    while True:
        time.sleep(TIME_BETWEEN_MEASUREMENTS)
        measurements_seconds += TIME_BETWEEN_MEASUREMENTS

        lat_list.append(droniada_data_source.position_gps.lat)
        lon_list.append(droniada_data_source.position_gps.lon)

        logger.critical("Calculated position: %f, %f", avg(lat_list), avg(lon_list))
        logger.info("Measuring for %i seconds", measurements_seconds)
