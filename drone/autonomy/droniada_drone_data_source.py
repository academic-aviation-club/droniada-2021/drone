import logging
import os
import time
from threading import Thread
from typing import Generator

from dronekit import VehicleMode

import drone.autonomy.config as config
from drone.cli import get_standard_cli_parser
from drone.drone_connection.mavlink import Mavlink, mavutil
from drone.nav.position_gps import Attitude, PositionGPS, PositionNED
from drone.setup_logging import setup_logging
from drone.telemetry.telemetry_sender import TelemetrySender

logger = logging.getLogger(__name__)

TIME_BETWEEN_HOUSEKEEPING_TELEMETRY = 2.5


class MavlinkDroneControlInterface:
    def __init__(self, mavlink_connection: Mavlink):
        self._mavlink = mavlink_connection

    def set_flight_destination(self, position: PositionGPS):
        self._mavlink.vehicle.groundspeed = config.GROUND_SPEED
        self._mavlink.fly_to_gps(position.lat, position.lon, position.altitude)
        msg = self._mavlink.vehicle.message_factory.command_long_encode(
            0,
            0,  # target system, target component
            mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
            0,  # confirmation
            config.HEADING,  # param 1, yaw in degrees
            0,  # param 2, yaw speed deg/s
            1,  # param 3, direction -1 ccw, 1 cw
            0,  # param 4, relative offset 1, absolute angle 0
            0,
            0,
            0,
        )  # param 5 ~ 7 not used
        # send command to vehicle

        self._mavlink.vehicle.send_mavlink(msg)

    def change_mode_rtl(self):

        msg = self._mavlink.vehicle.message_factory.command_long_encode(
            0,
            0,  # target system, target component
            mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
            0,  # confirmation
            config.LANDING_HEADING,
            0,  # param 2, yaw speed deg/s
            1,  # param 3, direction -1 ccw, 1 cw
            0,  # param 4, relative offset 1, absolute angle 0
            0,
            0,
            0,
        )  # param 5 ~ 7 not used
        # send command to vehicle
        self._mavlink.vehicle.send_mavlink(msg)
        time.sleep(2)

        self._mavlink.vehicle.mode = VehicleMode("RTL")

    def is_armable(self) -> bool:
        return bool(self._mavlink.vehicle.is_armable)

    def arm(self):
        self._mavlink.vehicle.armed = True
        self._mavlink.vehicle.mode = VehicleMode("GUIDED")

    def is_armed(self) -> bool:
        return bool(self._mavlink.vehicle.armed)

    def takeoff(self, altitude: int):
        self._mavlink.vehicle.simple_takeoff(altitude)


class DroniadaDataSource:
    def __init__(
        self,
        connection_string: str,
    ):
        self.telemetry_sender = TelemetrySender()

        self.mavlink_connection = Mavlink(
            connection_string,
            position_callback=self.ned_callback,
            gps_positon_callback=self.gps_callback,
            attitude_callback=self.attitude_callback,
            battery_callback=self.battery_callback,
            rangefinder_callback=self.rangefinder_callback,
        )

        self.drone_control_interface = MavlinkDroneControlInterface(
            self.mavlink_connection
        )

        self.position_gps = PositionGPS(0, 0, 0)
        self.position_ned = PositionNED(0, 0, 0)
        self.attitude = Attitude(0, 0, 0, 0)
        self.vehicle_mode = "UNKNOWN"  # Vehicle mode is unknown as of start
        self.mission_task_name = "UNKNOWN"  # Unknown as of start
        self.finished_command_timestamp: str = "UNKNOWN"
        self.launch_point = PositionGPS(0, 0, 0)
        self.battery_level = 0

        self.use_gps_alt = True # "NO_HEALTHCHECK" in os.environ
        logger.critical("Use gps alt is %r", self.use_gps_alt)

        self._new_data_from_mavlink = False

        self._housekeeping_thread = Thread(
            target=self._housekeeping_telemetry_function, args=(), name="Housekeeping"
        )
        self._housekeeping_thread.start()

    def wait_for_new_data(self):
        while True:
            if not self._new_data_from_mavlink:
                logger.warning("No new data from mavlink")
                time.sleep(0.1)
                continue
            else:
                break

    def ned_callback(self, north, east, down):
        logger.debug(f"North: {north} East:{east} Down: {down}")
        self.position_ned.north = north
        self.position_ned.east = east
        self.position_ned.down = down
        self._new_data_from_mavlink = True

    def gps_callback(self, lat, lon, alt):
        logger.debug(f"Lat: {lat} Lon:{lon} alt: {alt}")
        self.position_gps.lat = lat
        self.position_gps.lon = lon
        self.position_gps.altitude = alt

        self._new_data_from_mavlink = True

    def rangefinder_callback(self, veh, msg, rangefinder_alt):
        return
        # logger.info(f"Rangefinder value: {rangefinder_alt.distance}")
        # self.position_gps.altitude = rangefinder_alt.distance

    def attitude_callback(self, veh, msg, attitude):
        logger.debug(
            f"pitch: {attitude.pitch} yaw: {attitude.yaw} roll: {attitude.roll}, heading: {veh.heading}"
        )
        self.attitude.pitch = attitude.pitch
        self.attitude.yaw = attitude.yaw
        self.attitude.roll = attitude.roll
        self.attitude.heading = veh.heading
        self.vehicle_mode = str(veh.mode).replace(
            "VehicleMode:", ""
        )  # VehicleMode part is not needed
        self._new_data_from_mavlink = True

    def battery_callback(self, veh, msg, battery):
        logger.debug(f"battery: {battery}")
        self.battery_level = battery.voltage
        self._new_data_from_mavlink = True

    def _housekeeping_telemetry_function(self):
        while True:
            time.sleep(TIME_BETWEEN_HOUSEKEEPING_TELEMETRY)
            telem_queue_size = self.telemetry_sender.packets_to_send.qsize()
            logger.info("telem queue size: %i", telem_queue_size)

            # Dirty f00king h4ck
            if (
                self.position_ned.down == 0
                or self.position_ned.north == 0
                or self.position_ned.east == 0
            ):
                continue

            self.telemetry_sender.send_telemetry(
                {
                    "telemetry_queue_size": telem_queue_size,
                    "drone_position": [
                        self.position_gps.lon,
                        self.position_gps.lat,
                    ],
                    "attitude": {
                        "pitch": self.attitude.pitch,
                        "yaw": self.attitude.yaw,
                        "roll": self.attitude.roll,
                    },
                    "altitude": self.position_gps.altitude,
                    "vehicle_mode": self.vehicle_mode,
                    "mission_task": self.mission_task_name,
                    "finished_command_timestamp": self.finished_command_timestamp,
                    "battery_voltage": self.battery_level,
                }
            )


if __name__ == "__main__":
    # setup_logging()
    # args = get_standard_cli_parser().parse_args()

    # droniada_generator = DroniadaDataGenerator(
    #     args.connection_string, args.telemetry_url, args.drone_name
    # )

    # drone_data = droniada_generator.get_new_data()
    # while True:
    #     print(vars(next(drone_data)))
    pass
