import json
import logging
import os
import threading
import time
import traceback
from dataclasses import dataclass, field
from enum import Enum
from typing import List

import requests

from drone.autonomy.droniada_drone_data_source import DroniadaDataSource
from drone.camera.streamer_bzb_camera import GPhotoCameraControl
from drone.cli import get_standard_cli_parser
from drone.config import DRONE_COMMAND_URL
from drone.drop.drop_mechanism import Drop
from drone.nav.position_gps import PositionGPS
from drone.setup_logging import setup_logging
from drone.telemetry.telemetry_sender import TelemetrySender

from .drone_command import CommandType, DroneCommand

# from .mission import
from .mission_task import MissionTask, MissionTasks

logger = logging.getLogger(__name__)


class DroneController:
    """
    Highest-level drone controller. Does 2 things:

    1. Gets commands from remote server
    2. Starts/stops mission tasks which execute the commands from *1
    """

    def __init__(self, connection_string: str):

        self.current_drone_command = DroneCommand(
            command_timestamp=""
        )  # Idle by default, that's what we want
        self.droniada_data_source = DroniadaDataSource(connection_string)

        self.current_drone_task: MissionTask = MissionTasks.Idle(
            self.droniada_data_source, ""
        )
        self.telemetry_sender = TelemetrySender()
        self._drop = Drop()
        # uncomment to test baczek's healthcheck

    def run(self):
        self.current_drone_task.start()
        while True:
            # Don't make requests in a loop, you
            # might choke internet connection for other tasks
            time.sleep(2)
            drone_command_from_remote: DroneCommand = (
                self.get_drone_command_from_remote()
            )
            self.droniada_data_source.mission_task_name = (
                drone_command_from_remote.command_type.value
            )
            if drone_command_from_remote != self.current_drone_command:
                logger.critical(
                    "Switching to mission task %s!",
                    drone_command_from_remote.command_type,
                )

                logger.critical(
                    "Stopping mission task %s", self.current_drone_command.command_type
                )
                self.current_drone_task.stop()
                del self.current_drone_task  # Explicitly!

                self.current_drone_command = drone_command_from_remote
                self.current_drone_task = MissionTasks.from_command(
                    self.current_drone_command, self.droniada_data_source
                )

                logger.critical(
                    "Starting mission task %s",
                    self.current_drone_task.__class__.__name__,
                )
                self.current_drone_task.start()
            else:
                logger.debug("No mission task changes, continuing..")

    def get_drone_command_from_remote(self) -> DroneCommand:
        """Gets the drone command and formats it into a ``DroneCommand`` object"""
        logger.debug("Getting a new mission task")
        mission_task_json = DroneController._perform_mission_task_request()

        # TODO: data validation
        command_type = mission_task_json["command_type"]
        command_timestamp = mission_task_json["timestamp"]

        if command_type == CommandType.IDLE.value:
            logger.debug("Idle task received")
            return DroneCommand(command_timestamp, CommandType.IDLE)

        if command_type == CommandType.TAKEOFF.value:
            logger.debug("Takeoff task received")
            return DroneCommand(command_timestamp, CommandType.TAKEOFF)

        if command_type == CommandType.RTL.value:
            logger.debug("RTL task received")
            return DroneCommand(command_timestamp, CommandType.RTL)

        if command_type == CommandType.FLY_PATH.value:
            logger.debug("FLY_PATH task received")

            points_json = mission_task_json["points_to_fly_to"]
            points_to_fly_to = [
                PositionGPS(point_json["lat"], point_json["lon"], point_json["alt"])
                for point_json in points_json
            ]
            return DroneCommand(
                command_timestamp,
                command_type=CommandType.FLY_PATH,
                points_to_fly_to=points_to_fly_to,
            )

        if command_type == CommandType.SCAN.value:
            logger.debug("Scan task received")
            altitude = mission_task_json["scan_arguments"]["altitude"]
            polygon = mission_task_json["scan_arguments"]["polygon"]

            scan_data: List[PositionGPS] = []
            for point in polygon:
                lat = point["lat"]
                lon = point["lon"]
                scan_data.append(PositionGPS(lat, lon, altitude))

            return DroneCommand(
                command_timestamp, CommandType.SCAN, scan_data, altitude
            )

        if command_type == CommandType.DROP_PESTICIDES.value:
            logger.debug("Drop pesticides task received")

            points_json = mission_task_json["points_to_fly_to"]
            points_to_fly_to = [
                PositionGPS(point_json["lat"], point_json["lon"], point_json["alt"])
                for point_json in points_json
            ]

            # TODO: drop pesticides data
            return DroneCommand(
                command_timestamp,
                CommandType.DROP_PESTICIDES,
                points_to_fly_to=points_to_fly_to,
                pesticide_type=mission_task_json["pesticide_type"],
            )

        # If server returns an unknown value, go idle
        return DroneCommand(command_timestamp, CommandType.IDLE)

    @staticmethod
    def _perform_mission_task_request() -> dict:
        """Keeps on making a mission task request until success"""
        while True:
            # logger.info("Making a mission task request")
            time.sleep(0.2)  # Don't kill the CPU if the network becomes unavailable
            try:
                response = requests.get(DRONE_COMMAND_URL)
                if not response.status_code == 200:
                    logger.error(
                        "Non-200 status code %i received in response",
                        response.status_code,
                    )
                    continue

                json_response: dict = json.loads(response.text)
                return json_response

            except Exception:
                logger.error(
                    "Could not perform a mission task request: \n %s",
                    traceback.format_exc(),
                )

    def run_health_check(self):
        logger.critical("Running health check")
        camera = GPhotoCameraControl(self.droniada_data_source)
        logger.critical("camera control created")
        while True:
            time.sleep(3)
            logger.critical("taking a pic")
            photo_path = camera.take_photo(send_to_photo_sender=False)
            if os.path.exists(photo_path):
                logger.critical("Took a picture, healthcheck passed 1/2")
                break
            else:
                logger.critical("Could not take a picture, retrying")
                continue

        while True:
            time.sleep(3)
            try:
                result = requests.get(DRONE_COMMAND_URL)
            except Exception as e:
                logger.critical("could not connect to telemetry server: %s", str(e))
                continue

            if result.status_code == 200:
                logger.critical("Got drone command from remote, healthcheck passed 2/2")
                break
            else:
                logger.critical("Could not connect to telemetry server")
                continue

        logger.critical("Health check PASSED")


if __name__ == "__main__":
    setup_logging()

    args = get_standard_cli_parser().parse_args()
    kc = DroneController(args.connection_string)

    if "NO_HEALTHCHECK" not in os.environ:
        kc.run_health_check()

    kc.run()
