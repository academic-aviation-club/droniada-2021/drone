# import logging
# import time
# import traceback
# from copy import copy

# import drone.autonomy.config
# from drone.autonomy.droniada_context import DroniadaContext, PositionGPS, PositionNED
# from drone.autonomy.droniada_drone_data_generator import DroniadaDataGenerator
# from drone.camera.streamer_bzb_camera import GPhotoCameraControl
# from drone.cli import get_standard_cli_parser
# from drone.nav import ned_geo_conversion
# from drone.nav.area_divider import WaypointsCreator
# from drone.nav.sort_waypoints import SortWaypoints
# from drone.setup_logging import setup_logging

# logger = logging.getLogger(__name__)


# class Mission:
#     def __init__(self, drone_data_generator):
#         self.high_altitude_flight_points = list()
#         self.low_altitude_flight_points = list()
#         self._drone_data_generator = drone_data_generator
#         self.drone_data = DroniadaContext(None, None, None, None, None)  # type: ignore
#         self.get_drone_data_update()
#         self.camera = GPhotoCameraControl(self.drone_data)
#         self.launch_point: PositionGPS = PositionGPS(0, 0, 0)

#     def ned2geo(self, point: PositionNED):
#         return ned_geo_conversion.ned2geo(self.launch_point, point)

#     def geo2ned(self, point: PositionGPS):
#         return ned_geo_conversion.geo2ned(self.launch_point, point)

#     def get_drone_data_update(self):
#         self.drone_data = next(self._drone_data_generator)

#     def get_high_altitude_waypoints(self):
#         logger.info("Setting waypoints based on an area")
#         calc_waypoints = WaypointsCreator(
#             drone.autonomy.config.HIGH_FLIGHT_ALTITUDE, geojson_path="./area.json"
#         )
#         high_altitude_flight_points = calc_waypoints.run()
#         self.high_altitude_flight_points = high_altitude_flight_points

#     def sort_waypoints(self):
#         logger.info("Sorting waypoints")
#         sort = SortWaypoints(points=self.high_altitude_flight_points)
#         sorted = sort.run()
#         self.high_altitude_flight_points = sorted

#         for flight_point in self.high_altitude_flight_points:
#             flight_point_ned = self.geo2ned(flight_point)
#             self.drone_data.telemetry_sender.send_telemetry(
#                 {
#                     "high_scan_north": flight_point_ned.north,
#                     "high_scan_east": flight_point_ned.east,
#                     "high_scan_down": drone.autonomy.config.HIGH_FLIGHT_ALTITUDE,
#                 }
#             )

#     def fly_to_position(self, target_point: PositionGPS):

#         self.drone_data.drone_control.set_flight_destination(target_point)

#         while True:
#             self.get_drone_data_update()
#             distance_from_target_point = distance_between_gps_points(
#                 target_point, self.drone_data.position_gps
#             )
#             logger.info("Distance from target point: %f", distance_from_target_point)
#             if (
#                 distance_from_target_point
#                 < drone.autonomy.config.GPS_POSITION_EPSILON_METERS
#             ):
#                 break

#     def take_photo(self) -> str:
#         # self._prepare_before_taking_photo()
#         return self.camera.take_photo()

#     def _prepare_before_taking_photo(self):
#         time1 = time.time()
#         while True:
#             self.get_drone_data_update()
#             time2 = time.time()
#             loop_time = time2 - time1
#             angle = abs(self.drone_data.attitude.pitch) + abs(
#                 self.drone_data.attitude.roll
#             )
#             if (
#                 angle < drone.autonomy.config.MAX_ANGLE
#                 or loop_time > drone.autonomy.config.PHOTO_WAIT_TIME
#             ):
#                 break

#     def high_altitude_scan(self):
#         while len(self.high_altitude_flight_points) > 0:
#             current_target_point = self.high_altitude_flight_points.pop()
#             self.fly_to_position(current_target_point)

#             try:
#                 logger.info("Taking a picture")
#                 image_path = self.take_photo()

#                 logger.info("Sending picture to Robur")
#                 result = self.robur.ask_robur(image_path)
#                 self.drone_data.telemetry_sender.send_telemetry(
#                     {"num_of_detections": len(result.objects_list)}
#                 )

#                 for detection in result.objects_list:
#                     self.drone_data.telemetry_sender.send_telemetry(
#                         {
#                             "detection_id": detection.classId,
#                             "detection_area": detection.area,
#                         }
#                     )

#             except:
#                 logger.info("Robienie zdjęcia się wyjebało, %s", traceback.format_exc())
#                 self.drone_data.telemetry_sender.send_telemetry(
#                     {"picture_pipeline_error": 1}
#                 )

#     def low_altitude_scan(self):
#         while len(self.low_altitude_flight_points) > 0:
#             current_target_point = self.low_altitude_flight_points.pop()
#             current_target_point.altitude = drone.autonomy.config.LOW_FLIGHT_ALTITUDE
#             self.fly_to_position(current_target_point)

#             logger.info("Taking a picture")
#             image_path = self.take_photo()

#             logger.info("Sending picture to Robur")
#             self.robur.ask_robur(image_path)

#     def return_to_launch(self):
#         logger.critical("Mode RTL")
#         self.drone_data.drone_control.change_mode_rtl()

#     def fly(self):
#         self.get_high_altitude_waypoints()
#         self.sort_waypoints()
#         self.takeoff_and_wait()
#         self.high_altitude_scan()
#         self.return_to_launch()


# if __name__ == "__main__":
#     setup_logging()
#     args = get_standard_cli_parser().parse_args()

#     droniada_generator = DroniadaDataGenerator(args.connection_string)

#     drone_data = droniada_generator.get_new_data()
#     mission = Mission(drone_data)
#     mission.fly()
