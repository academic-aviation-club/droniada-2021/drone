from dataclasses import dataclass, field
from enum import Enum
from typing import List

from drone.nav.position_gps import PositionGPS


class CommandType(Enum):
    IDLE = "IDLE"
    TAKEOFF = "TAKEOFF"
    SCAN = "SCAN"
    FLY_PATH = "FLY_PATH"
    DROP_PESTICIDES = "DROP_PESTICIDES"
    RTL = "RTL"


@dataclass
class DroneCommand:
    command_timestamp: str
    command_type: CommandType = CommandType.IDLE
    scan_data: List[PositionGPS] = field(default_factory=list)
    scan_altitude: int = 5
    points_to_fly_to: List[PositionGPS] = field(default_factory=list)
    pesticide_type: str = "yellow"
