import logging
import time
from os import execve
from threading import Event

from drone.autonomy.config import (
    GPS_POSITION_EPSILON_METERS,
    GPS_POSITION_EPSILON_METERS_SCAN,
)
from drone.autonomy.droniada_drone_data_source import DroniadaDataSource
from drone.nav.position_gps import PositionGPS, distance_between_gps_points

logger = logging.getLogger(__name__)


def fly_to_point(
    target_point: PositionGPS,
    data_source: DroniadaDataSource,
    task_end: Event,
    epsilon_meters: float = GPS_POSITION_EPSILON_METERS_SCAN,
):
    while True:

        if task_end.is_set():
            return

        data_source.drone_control_interface.set_flight_destination(target_point)
        time.sleep(1)
        data_source.wait_for_new_data()
        distance_from_target_point = distance_between_gps_points(
            target_point, data_source.position_gps
        )
        logger.info("Distance from target point: %f", distance_from_target_point)
        if distance_from_target_point < epsilon_meters:
            logger.critical("WAYPOINT REACHED")
            return
