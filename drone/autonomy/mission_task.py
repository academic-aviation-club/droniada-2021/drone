import logging
import random
import threading
import time
from abc import ABC, abstractmethod
from copy import copy
from typing import List

import drone.autonomy.config
from drone.autonomy.config import (
    GPS_POSITION_EPSILON_METERS,
    GPS_POSITION_EPSILON_METERS_DROP,
    GPS_POSITION_EPSILON_METERS_SCAN,
)
from drone.autonomy.droniada_drone_data_source import DroniadaDataSource
from drone.camera.streamer_bzb_camera import GPhotoCameraControl
from drone.drop.drop_mechanism import Drop
from drone.nav.area_divider import WaypointsCreator
from drone.nav.position_gps import (
    Attitude,
    PositionGPS,
    PositionNED,
    distance_between_gps_points,
)
from drone.nav.sort_waypoints import SortWaypoints
from drone.telemetry.telemetry_sender import TelemetrySender

from . import utils
from .drone_command import CommandType, DroneCommand

logger = logging.getLogger()


class MissionTask(ABC):
    """
    When a new directive is received from a server, a mission task is started.
    It performs a specific mission task:
    - Being idle
    - Scanning a specific polygon
    - Dropping a ball at a specific location
    - RTLing

    A mission task should quit when a ``task_end`` event is received
    """

    def __init__(self, data_source: DroniadaDataSource, command_timestamp: str):
        self.task_end = threading.Event()
        self.data_source = data_source
        self.command_timestamp = command_timestamp

        self.task_thread = threading.Thread(
            name=self.__class__.__name__,
            target=self._run_thread_function,
        )

    def start(self):
        self.task_thread.start()

    def stop(self):
        logger.info("Stopping mission task %s", self.__class__.__name__)

        self.task_end.set()
        self.task_thread.join()

        logger.critical("Updating finished command timestamp")
        self.data_source.finished_command_timestamp = self.command_timestamp

    def _run_thread_function(self):
        self._run()

        logger.critical("Updating finished command timestamp")
        self.data_source.finished_command_timestamp = self.command_timestamp

        while True:
            if self.task_end.is_set():
                return
            time.sleep(1)

    @abstractmethod
    def _run(self):
        """Runs the thread, implemented by subclasses that actually DO STUFF"""


class MissionTasks:
    """Logic container storing all mission tasks"""

    @staticmethod
    def from_command(
        command: DroneCommand, data_source: DroniadaDataSource
    ) -> MissionTask:
        """Utility function, using a DroneCommand parsed from the API to construct a MissionTask"""
        if command.command_type == CommandType.IDLE:
            return MissionTasks.Idle(data_source, command.command_timestamp)

        if command.command_type == CommandType.TAKEOFF:
            return MissionTasks.Takeoff(data_source, command.command_timestamp)

        if command.command_type == CommandType.RTL:
            return MissionTasks.ReturnToLaunch(data_source, command.command_timestamp)

        if command.command_type == CommandType.FLY_PATH:
            return MissionTasks.FlyPath(
                data_source, command.command_timestamp, command.points_to_fly_to
            )

        if command.command_type == CommandType.DROP_PESTICIDES:
            return MissionTasks.DropPesticides(
                data_source,
                command.points_to_fly_to[0],
                command.pesticide_type,
                command.command_timestamp,
            )

        if command.command_type == CommandType.SCAN:
            waypoints_creator = WaypointsCreator(
                altitude=command.scan_altitude, points=command.scan_data
            )
            waypoints = waypoints_creator.run()
            sort_waypoints = SortWaypoints(points=waypoints)
            sorted_waypoints = sort_waypoints.run()
            return MissionTasks.Scan(
                data_source, command.command_timestamp, sorted_waypoints
            )

        # If something unknown was returned, go idle
        return MissionTasks.Idle(data_source, command.command_timestamp)

    class Idle(MissionTask):
        """
        Used at the start of the mission, or when waiting for some external processing to finish.
        Literally does nothing :)
        """

        def _run(self):
            self.camera_control = GPhotoCameraControl(self.data_source)

            while not self.task_end.is_set():
                logger.info("Running on idle, not doing anything")
                time.sleep(1)

                # if self.data_source.position_gps.altitude > 10:
                # self.camera_control.take_photo()

            logger.critical("LEAVING IDLE")

    class Takeoff(MissionTask):
        """
        Waits for the vehicle to become armable, then takes off.

        **Important note: this task won't exit until the drone takes off, be careful with it!!**
        """

        def _run(self):
            while not self.data_source.drone_control_interface.is_armable():
                logger.info("Waiting for vehicle to become armable")
                time.sleep(1)
                self.data_source.wait_for_new_data()

            while not self.data_source.drone_control_interface.is_armed():
                logger.critical("ARMING")
                self.data_source.drone_control_interface.arm()
                self.data_source.wait_for_new_data()
                time.sleep(0.5)

            # TODO: do something with it
            self.data_source.launch_point = copy(self.data_source.position_gps)

            logger.critical("Taking off")
            self.data_source.drone_control_interface.takeoff(
                drone.autonomy.config.TAKEOFF_ALTITUDE
            )

    class FlyPath(MissionTask):
        """Flies to the indicated point."""

        def __init__(
            self,
            data_source: DroniadaDataSource,
            command_timestamp: str,
            route: List[PositionGPS],
        ):
            super().__init__(data_source, command_timestamp)
            self.camera_control = GPhotoCameraControl(self.data_source)
            self.route = route

        def _run(self):
            for target_point in self.route:
                utils.fly_to_point(target_point, self.data_source, self.task_end)
                time.sleep(3)
                self.camera_control.take_photo()

                if self.task_end.is_set():
                    return

            logger.critical("Flight along path finished")

    class DropPesticides(MissionTask):
        """Flies to the indicated point."""

        # TODO
        def __init__(
            self,
            data_source: DroniadaDataSource,
            tree_position: PositionGPS,
            pesticide_type: str,
            command_timestamp: str,
        ):
            super().__init__(data_source, command_timestamp)
            self.camera_control = GPhotoCameraControl(self.data_source)
            self.tree_position = tree_position
            self.drop_mechanism = Drop()
            self.pesticide_type = pesticide_type
            self.telemetry_sender = TelemetrySender()

        def _run(self):
            utils.fly_to_point(
                self.tree_position,
                self.data_source,
                self.task_end,
                epsilon_meters=GPS_POSITION_EPSILON_METERS_DROP,
            )
            logger.critical("ON POSITION, DROPPING")
            time.sleep(2)
            if self.pesticide_type == "yellow":
                logger.critical("Dropping yellow ball")
                self.drop_mechanism.drop_yellow_ball()
            else:
                logger.critical("Dropping orange ball")
                self.drop_mechanism.drop_orange_ball()

            self.telemetry_sender.send_telemetry(
                {
                    "pesticide_drop": {
                        "location": [self.tree_position.lon, self.tree_position.lat],
                        "pesticide": self.pesticide_type,
                    }
                }
            )

    class Scan(MissionTask):
        """Flies through all points from list."""

        def __init__(
            self,
            data_source: DroniadaDataSource,
            command_timestamp: str,
            scan_points: List[PositionGPS],
        ):
            super().__init__(data_source, command_timestamp)
            self.scan_points = scan_points
            self.camera_control = GPhotoCameraControl(self.data_source)
            self.telemetry_sender = TelemetrySender()

        def _run(self):
            for target_point in self.scan_points:
                # HERE KURWA
                utils.fly_to_point(target_point, self.data_source, self.task_end)
                logger.critical("WAYPOINT REACHED")
                time.sleep(1)
                self.camera_control.take_photo()

            logger.critical("SCANNING COMPLETE")

    class ReturnToLaunch(MissionTask):
        """
        Switches the drone into RTL

        **Important note: this mission task is not stoppable**
        """

        def _run(self):
            self.data_source.drone_control_interface.change_mode_rtl()
            while True:
                if self.data_source.position_gps.altitude < 1:
                    logger.info("Returned to launch!")
                else:
                    logger.info("Returning to launch..")

                time.sleep(5)
