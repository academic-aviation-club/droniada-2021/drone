import json
import logging
import queue
import time
import traceback
from dataclasses import dataclass
from queue import Queue
from threading import Thread

import requests

from drone.camera.Photo import Photo
from drone.config import SEND_PHOTO_URL
from drone.setup_logging import setup_logging

IMAGE_PATH = "/home/olga/code/drone/drone/camera/test.jpg"
TIMEOUT = 20
MAX_SEND_PHOTO_RETRIES = 3
logger = logging.getLogger(__name__)


class PhotoSender:
    def __init__(self, url=SEND_PHOTO_URL):
        self.url = url
        self.image_sending = Thread(target=self._send_photos_thread, args=())
        self.photos_queue: Queue = Queue()

    def start(self):
        self.image_sending.start()

    def _send_photos_thread(self):
        logger.info("Start photos thread")

        while True:
            logger.info(f"Photo queue size: {self.photos_queue.qsize()}")
            try:
                photo_to_send: Photo = self.photos_queue.get()

                time_photo_sending_start = time.time()
                self._send_photo(photo_to_send)
                time_photo_sending_end = time.time()

                logger.info(
                    "Photo sending took %f seconds",
                    time_photo_sending_end - time_photo_sending_start,
                )
            except:
                logger.error(f"Error occured: {traceback.format_exc()}")

    def _send_photo(self, photo: Photo):
        logger.info("Start photo sending")
        with open(photo.image_path, "rb") as image_file:
            image_data = image_file.read()
            files = {
                "file": image_data,
            }
            data = {
                "lat": photo.lat,
                "lon": photo.lon,
                "alt": photo.alt,
                "heading": photo.heading,
                "pitch": photo.pitch,
                "yaw": photo.yaw,
                "roll": photo.roll,
                # 'timestamp': photo.timestamp
            }

            for i in range(MAX_SEND_PHOTO_RETRIES):
                try:
                    response = requests.post(
                        self.url,
                        files=files,
                        data=data,
                        timeout=TIMEOUT,
                    )
                    response.raise_for_status()
                    logger.info(f"status code: {response.status_code}")
                    logger.info(f"Response: {response.text}")
                    break  # Exit the loop if file sending was successfull
                except requests.HTTPError:
                    logger.error("Could not send picture, retrying")

            return response

    def send_photo(self, photo: Photo):
        self.photos_queue.put(photo)
        logger.info(f"{photo.image_path} put in the queue")


if __name__ == "__main__":
    setup_logging()
    ps = PhotoSender()
    ps.start()
