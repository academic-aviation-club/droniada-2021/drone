from dataclasses import dataclass
from typing import List


@dataclass
class DetectedObject:
    x: int = 0
    y: int = 0
    w: int = 0
    h: int = 0
    classId: int = 0
    className: str = ""
    confidence: float = 0.0
    area: float = 0.0
    pos_x: float = 0.0
    pos_y: float = 0.0


class Detection:
    def __init__(
        self, successful: bool, objects_list: List[DetectedObject], filename: str
    ):
        self.successful = successful
        self.objects_list = objects_list
        self.filename = filename
