# type: ignore
import json
import logging
import os
import time
import traceback

import requests
from PIL import Image
from requests.exceptions import HTTPError

from drone.camera.send_photos import IMAGE_PATH

"""
Olga's stuff dont touch!!!
"""
# IMAGE_PATH = "/home/olga/try.jpg"

FOCAL_LENGTH = 20.0
SENSORW = 23.5
SENSORH = 15.6

URL = "http://79.189.78.141:9999/predictDroniada"
IMAGE_DIR = "/home/olga/Downloads/Olga"
RESIZED_DIR = "/home/olga/Downloads/olga_resized"
# foo = Image.open(IMAGE_PATH)
# print(foo.size)
# foo = foo.resize((900,600),Image.ANTIALIAS)
# foo.save("/home/olga/image_scaled.jpg",quality=100)
# print(foo.size)
# foo.save(f"/home/olga/image_scaled_opt.jpg",optimize=True,quality=100)
# print(foo.size)

logger = logging.getLogger(__name__)


def lower_resolution(x: int, y: int, image_name: str) -> str:
    original_image = Image.open(os.path.join(IMAGE_DIR, image_name))
    image_resized_path = os.path.join(RESIZED_DIR, image_name)
    resized_image = original_image.resize((x, y), Image.ANTIALIAS)
    resized_image.save(image_resized_path, optimized=True, quality=100)

    return image_resized_path


def ask_robur(image_path: str, timeout=30, max_retries_num=1):
    print("ask robur")
    files = {"image": open(image_path, "rb")}
    data = {
        "lat": 51.232,
        "lon": 52.4343,
        "alt": 50.0,
        "heading": 0,
        "pitch": 0.0,
        "yaw": 0.0,
        "roll": 0.0,
        "focal_length": FOCAL_LENGTH,
        "sensorw": SENSORW,
        "sensorh": SENSORH,
    }
    response = None
    for i in range(0, max_retries_num):
        logger.info(f"Trying to send the request for {i} time")
        try:
            logger.info("Sending post request")
            response = requests.post(url=URL, files=files, data=data, timeout=timeout)
            response.raise_for_status()
            logger.info(f"Got response from server {response.status_code}")
            return response
        except HTTPError as http_err:
            logger.error(f"Http error {http_err}")
        except requests.exceptions.Timeout:
            logger.error("Connection timed out")
        except:
            logger.error("Robur asking failed: %s", traceback.format_exc())
    return None


def process_robur_response(response) -> None:
    print("process robur response")
    logger.info("Start processing response")
    new_string = response.replace("\\", "")
    response_dict = json.loads(new_string)
    print(json.dumps(response_dict))
    filename = response_dict["filename"]
    detections = response_dict["Detections"]
    logger.info(f"Detections number {len(detections)}")
    return len(detections)
    # if not detections:
    # logger.info(f"Nothing detected filename {filename}")

    # for obj in detections:
    #     obj["filename"] = filename
    #     detection = dacite.from_dict(data_class=Detection, data=obj)
    #     self.detections_queue.put(detection)
    #     logger.info(f"Object added to queue {detection}")


def checkout_all_files(dir_path: str, x: int, y: int):
    for filename in os.listdir(dir_path):
        if filename.endswith(".jpg"):
            image_path = os.path.join(dir_path, filename)
            resized_image_path = lower_resolution(x, y, image_name=filename)
            start = time.time_ns()
            robur_response_original = ask_robur(image_path=image_path)
            print("original", time.time_ns() - start)
            start_resized = time.time_ns()
            robur_response_resized = ask_robur(image_path=resized_image_path)
            print("resized", time.time_ns() - start_resized)

            if (
                robur_response_original is not None
                and robur_response_resized is not None
            ):
                detection_num_original_image = process_robur_response(
                    robur_response_original.text
                )
                detection_num_resized_image = process_robur_response(
                    robur_response_resized.text
                )
                print(
                    f"ORIGINAL: {detection_num_original_image} RESIZED: {detection_num_resized_image}"
                )
        else:
            continue


def run():
    checkout_all_files(dir_path=IMAGE_DIR, x=900, y=600)


if __name__ == "__main__":
    run()
