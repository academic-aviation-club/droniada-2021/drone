import copy
import datetime
import json
import logging
import os
import sys
import time
import traceback
import zlib
from dataclasses import asdict
from threading import Lock, Thread
from typing import Dict

import piexif
from piexif import helper
from PIL import Image

from drone import config
from drone.autonomy.droniada_drone_data_source import DroniadaDataSource
from drone.camera.Photo import Photo
from drone.camera.send_photos import PhotoSender
from drone.cli import get_standard_cli_parser
from drone.drone_connection.mavlink import Mavlink
from drone.nav.position_gps import Attitude, PositionGPS, PositionNED
from drone.setup_logging import setup_logging
from drone.telemetry.telemetry_sender import Singleton, TelemetrySender

IMAGE_SAVE_PATH = os.path.dirname(__file__)

logger = logging.getLogger(__name__)


class GPhotoCameraControl(metaclass=Singleton):
    def __init__(self, data_source: DroniadaDataSource):
        self.data_source = data_source
        self.telemetry_sender = TelemetrySender()
        self.photo_sender = PhotoSender()
        self.photo_sender.start()

    def take_photo(
        self,
        res_x: int = config.CAMERA_PHOTO_SIZE_X,
        res_y: int = config.CAMERA_PHOTO_SIZE_Y,
        send_to_photo_sender=True,
    ) -> str:
        """Take a picture and return a path to it"""
        try:
            logger.info("Start taking photo")
            timestamp = int(time.time())
            image_name = os.path.join(IMAGE_SAVE_PATH, f"image_{timestamp}.jpg")
            # metadata_name = os.path.join(IMAGE_SAVE_PATH, f"image_{timestamp}.txt")
            attitude = copy.copy(self.data_source.attitude)
            gps = copy.copy(self.data_source.position_gps)

            os.system(
                f"gphoto2 --trigger-capture --wait-event-and-download=FILEADDED --filename {image_name}"
            )

            if not res_x == 0 and not res_y == 0:
                GPhotoCameraControl.lower_resolution(
                    x=res_x, y=res_y, image_path=image_name
                )
            # with open(metadata_name, "w") as file:
            #     file.write(str(asdict(attitude)))
            #     file.write("\n")
            #     file.write(str(asdict(gps)))

            # logger.critical("eluwina")
            # return ""

            logger.info("Photo taken")
            photo = Photo(
                image_path=image_name,
                lat=str(gps.lat),
                lon=str(gps.lon),
                alt=str(gps.altitude),
                heading=str(attitude.heading),
                pitch=str(attitude.pitch),
                yaw=str(attitude.yaw),
                roll=str(attitude.roll),
                timestamp=str(timestamp),
            )
            self.telemetry_sender.send_telemetry({"photo_event": asdict(photo)})
            self.telemetry_sender.send_telemetry(
                {"photo_taken": [photo.lon, photo.lat]}
            )

            if send_to_photo_sender:
                self.photo_sender.send_photo(photo=photo)
        except:
            logger.error(
                f"Some errored occure while taking photo: {traceback.format_exc()}"
            )

        return image_name

    @staticmethod
    def lower_resolution(x: int, y: int, image_path: str) -> None:
        logger.info(f"Resizing image to {x}, {y}")
        original_image = Image.open(image_path)
        resized_image = original_image.resize((x, y), Image.ANTIALIAS)
        resized_image.save(image_path, optimized=True, quality=100)

        return

    @staticmethod
    def stamp_photo(image_path, metadata: dict):
        exif_dict = piexif.load(image_path)

        exif_dict["Exif"][piexif.ExifIFD.UserComment] = helper.UserComment.dump(
            json.dumps(metadata), encoding="unicode"
        )
        piexif.insert(piexif.dump(exif_dict), image_path)

    @staticmethod
    def get_image_metadata(image_path):
        exif_dict = piexif.load(image_path)
        gps_data = piexif.helper.UserComment.load(
            exif_dict["Exif"][piexif.ExifIFD.UserComment]
        )
        return json.loads(gps_data)


# if __name__ == "__main__":
#     setup_logging()
#     droniada_data_source = DroniadaDataSource("0.0.0.0:14550")
#     gphoto_control = GPhotoCameraControl(data_source=droniada_data_source)

#     while True:
#         time.sleep(3)
#         gphoto_control.take_photo()
