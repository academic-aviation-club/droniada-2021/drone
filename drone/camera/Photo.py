from dataclasses import dataclass


@dataclass
class Photo:
    image_path: str
    lat: str
    lon: str
    alt: str
    heading: str
    pitch: str
    yaw: str
    roll: str
    timestamp: str
