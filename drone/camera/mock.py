import requests


class CameraMock:
    def __init__(self, url="http://localhost:5000"):
        self.url = url

    def take_picture(self, north, east, down, heading):
        request_data = {"north": north, "east": east, "down": down, "heading": heading}

        response = requests.post(self.url, data=request_data)
        if response.status_code == 200:
            return response.content
        else:
            raise ValueError(
                "Wrong status code!! Are the params okay? Is the server running?"
            )
