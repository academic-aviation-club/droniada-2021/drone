import json
import logging
import threading
import time
import traceback
from dataclasses import dataclass, field
from datetime import datetime
from getpass import getuser
from queue import Queue
from typing import Dict

import requests

from drone.config import TELEMETRY_ENDPOINT

logger = logging.getLogger(__name__)


class Singleton(type):
    lock = threading.Lock()
    _instances: Dict[type, object] = {}

    def __call__(cls, *args, **kwargs):
        with Singleton.lock:
            if cls not in cls._instances:
                cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)

        return cls._instances[cls]


class TelemSenderSingleton(type):
    lock = threading.Lock()
    _instances: Dict[type, object] = {}

    def __call__(cls, *args, **kwargs):
        with TelemSenderSingleton.lock:
            if cls not in cls._instances:
                cls._instances[cls] = super(TelemSenderSingleton, cls).__call__(
                    *args, **kwargs
                )

        return cls._instances[cls]


@dataclass
class TelemetryPacket:
    """
    Packet: utc timestamp + measurement/event data.
    All standard types in the dicts should work,
    but we should probably stick to numbers
    """

    timestamp: datetime
    data: dict = field(default_factory=dict)


class TelemetrySender(metaclass=TelemSenderSingleton):
    """
    Sends telemetry in a non-blocking way
    via a separate thread doing the requests
    """

    def __init__(
        self,
    ):
        logger.critical(
            "Creating a new telemetry sender singleton. Only one should be created!"
        )
        self.drone_name = getuser()
        self.url = TELEMETRY_ENDPOINT

        self.packets_to_send: Queue[TelemetryPacket] = Queue()

        self.telemetry_sender_thread = threading.Thread(
            target=self._telemetry_sender_thread_function, name="Telemetry"
        )

        self.telemetry_sender_thread.start()

    def send_telemetry(self, data: dict):
        """
        The only function you will ever need from this class. Non-blocking!
        """
        packet = TelemetryPacket(datetime.utcnow(), data)
        self.packets_to_send.put(packet)

    def _telemetry_sender_thread_function(self):
        logger.info("Telemetry sender thread started")
        while True:
            try:
                packet: TelemetryPacket = self.packets_to_send.get(block=True)
                timestamp = datetime.timestamp(packet.timestamp)
                data_to_send = packet.data

                # TODO
                logger.debug("Sending %s %s", str(timestamp), str(data_to_send))

                if "timestamp" in data_to_send:
                    logger.critical(
                        "`timestamp` key present in data_to_send, overriding"
                    )

                data_to_send["timestamp"] = timestamp
                data_to_send["drone_name"] = self.drone_name

                self._send_packet_until_success(data_to_send)

            except Exception:  # pylint: disable=W0703
                logger.error(
                    "Exception in the telemetry server thread:\n%s",
                    traceback.format_exc(),
                )

    def _send_packet_until_success(self, data_to_send: dict):
        while True:
            try:
                time.sleep(0.1)
                telemetry_send_result = requests.post(
                    self.url, data=json.dumps(data_to_send)
                )
                if telemetry_send_result.status_code == 200:
                    return
            except Exception:  # pylint: disable=W0703
                logger.error(
                    "Exception in the telemetry server thread:\n%s",
                    traceback.format_exc(),
                )
                time.sleep(0.5)
