import logging
import platform
from dataclasses import dataclass
from time import sleep

from drone.telemetry.telemetry_sender import Singleton

logger = logging.getLogger(__name__)

TIME_TO_MOVE_SERVO = 1


@dataclass
class ServoConfig:
    PIN_NUMBER: int
    OPEN_POSITION: float
    CLOSE_POSITION: float


OrangeServoConfig = ServoConfig(PIN_NUMBER=13, OPEN_POSITION=-0.5, CLOSE_POSITION=0.8)
YellowServoConfig = ServoConfig(PIN_NUMBER=12, OPEN_POSITION=0.0, CLOSE_POSITION=-1)

if platform.machine() == "x86_64":

    logger.critical("Using mock Servo")

    class Servo:
        def __init__(self, pin_number):
            self.pin_number = pin_number
            self.value = 0.0


else:
    logger.critical("Using real Servo")
    from gpiozero import Servo  # type: ignore


class Drop(metaclass=Singleton):
    def __init__(self):
        self.servo_orange = Servo(OrangeServoConfig.PIN_NUMBER)
        self.servo_yellow = Servo(YellowServoConfig.PIN_NUMBER)

        self.close_orange()
        self.close_yellow()
        sleep(TIME_TO_MOVE_SERVO)

    def drop_yellow_ball(self):
        self.open_yellow()
        sleep(TIME_TO_MOVE_SERVO)
        self.close_yellow()
        sleep(TIME_TO_MOVE_SERVO)

    def drop_orange_ball(self):
        self.open_orange()
        sleep(TIME_TO_MOVE_SERVO)
        self.close_orange()
        sleep(TIME_TO_MOVE_SERVO)

    def open_yellow(self):
        self._open(self.servo_yellow, YellowServoConfig)

    def close_yellow(self):
        self._close(self.servo_yellow, YellowServoConfig)

    def open_orange(self):
        self._open(self.servo_orange, OrangeServoConfig)

    def close_orange(self):
        self._close(self.servo_orange, OrangeServoConfig)

    def _open(self, servo: Servo, config: ServoConfig):
        servo.value = config.OPEN_POSITION

    def _close(self, servo: Servo, config: ServoConfig):
        servo.value = config.CLOSE_POSITION


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("--init", help="Only initialise servos", action="store_true")
    parser.add_argument("--yellow", help="Drop yellow ball", action="store_true")
    parser.add_argument("--orange", help="Drop orange ball", action="store_true")

    args = parser.parse_args()

    if args.init:
        drop = Drop()

    if args.yellow:
        drop = Drop()
        drop.drop_yellow_ball()

    if args.orange:
        drop = Drop()
        drop.drop_orange_ball()
