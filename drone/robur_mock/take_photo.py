from logging import currentframe

import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from drone.nav.area_divider import WaypointsCreator
from drone.nav.position_gps import PositionGPS

objects = [
    [52.23804346310477, 16.24305009841919, 0],
    [52.23801061234779, 16.24274969100952, 0],
    [52.237530988526785, 16.242706775665283, 0],
    [52.23680168757893, 16.240400075912476, 0],
    [52.238240567136025, 16.24056100845337, 0],
    [52.236887101823, 16.240206956863403, 0],
    [52.2370447892264, 16.240593194961548, 0],
    [52.23773466502915, 16.24127984046936, 0],
    [52.236506021618595, 16.242095232009888, 0],
    [52.23757698007677, 16.240142583847046, 0],
]


def get_photo_area(current_drone_location: PositionGPS) -> list[PositionGPS]:
    """
    Real dimensions of area to be photographed.
    """
    waypoint_creator = WaypointsCreator(current_drone_location.altitude)
    photo_area = waypoint_creator.get_rectangle(current_drone_location)
    return photo_area


def is_in_geofence(photo_area: list[PositionGPS], point: PositionGPS) -> bool:
    """
    Checks if point is inside or outside of geofence.
    """
    temp_points = list()
    for temp_point in photo_area:
        temp_points.append((temp_point.lat, temp_point.lon))

    lons_lats_vect = np.asarray(temp_points)
    polygon = Polygon(lons_lats_vect)
    point = Point(point.lat, point.lon)
    return bool(polygon.contains(point))


class Robur:
    def __init__(self, detected_objects=objects) -> None:
        self.detected_objects = detected_objects

    def get_objects_in_photo(self, photo_area) -> list[PositionGPS]:
        """
        Returns the objects visible in the photo.
        """
        detected_objects_in_photo = list()
        for point in self.detected_objects:
            pointGPS = PositionGPS(point[0], point[1], point[2])
            if is_in_geofence(photo_area, pointGPS):
                detected_objects_in_photo.append(pointGPS)
        return detected_objects_in_photo

    def take_photo(self, current_drone_location) -> list[PositionGPS]:
        photo_area = get_photo_area(current_drone_location)
        detected_objects = self.get_objects_in_photo(photo_area)
        return detected_objects


if __name__ == "__main__":
    pass
    # point = PositionGPS(52.236499451241535, 16.24219179153442, 50)
    # robur = Robur()
    # detected_objects = robur.take_photo(point)
    # print(len(detected_objects))
