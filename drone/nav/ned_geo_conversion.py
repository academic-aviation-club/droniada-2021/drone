import math

from .position_gps import PositionGPS, PositionNED


def geo2ned(reference_point: PositionGPS, point: PositionGPS) -> PositionNED:
    north = (point.lat - reference_point.lat) * (40075704.0 / 360)
    east = (
        math.cos(reference_point.lat)
        * (point.lon - reference_point.lon)
        * (40075704.0 / 360)
    )
    return PositionNED(north, east, point.altitude)


def ned2geo(reference_point: PositionGPS, point: PositionNED) -> PositionGPS:
    lat = (point.north / (40075704.0 / 360)) + reference_point.lat
    lon = (
        point.east / (math.cos(math.radians(reference_point.lat)) * (40075704.0 / 360))
    ) + reference_point.lon
    return PositionGPS(lat, lon, point.down)
