class Camera:
    def __init__(
        self,
        focal_lenght: float,
        sensor_width: float,
        sensor_height: float,
        res_x: int,
        res_y: int,
    ):
        self.focal_lenght = focal_lenght
        self.sensor_width = sensor_width
        self.sensor_height = sensor_height
        self.res_x = res_x
        self.res_y = res_y

    def get_real_width(self, altitude: float) -> float:
        """
        Real photo width [m]
        """

        return (altitude * self.sensor_width) / (self.focal_lenght)

    def get_real_height(self, altitude: float) -> float:
        """
        Real photo height [m]
        """

        return (altitude * self.sensor_height) / (self.focal_lenght)
