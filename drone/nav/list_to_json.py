import json
import os
from typing import List


def save_json(points_list, file_name):

    list_of_points_to_json: List[List[float]] = []
    for point in points_list:
        list_of_points_to_json.append([point.lat, point.lon, point.altitude])

    POINTS = {"type": "PointsCollection", "points": list_of_points_to_json}

    path = os.path.join("./", file_name)
    with open(path, "w") as f:
        print(path)
        json.dump(POINTS, f, indent=2)
