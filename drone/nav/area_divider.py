import json
import math
from typing import List

import numpy as np
from geopy import distance
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

import drone.nav.config as config
import drone.nav.list_to_json as list_to_json
from drone.nav.position_gps import PositionGPS as GeoPoint
from drone.nav.util_img2geo import Camera


def north_to_lat(n: float, lat: float) -> float:
    """
    Converts north shift to a latitude
    """
    return (n / (40075704.0 / 360)) + lat


def east_to_lon(e: float, lat: float, lon: float) -> float:
    """
    Converts east shift to latitude and longitude
    """
    lat = math.radians(lat)
    return (e / (math.cos(lat) * (40075704.0 / 360.0))) + lon


def find_mid_point(area: List[GeoPoint]) -> GeoPoint:
    """
    Calculates the arithmetic mean of the points.
    """
    lat_sum: float = 0.0
    lon_sum: float = 0.0
    for point in area:
        lat_sum += point.lat
        lon_sum += point.lon
    mid_point = GeoPoint(lat_sum / len(area), lon_sum / len(area), 0)
    return mid_point


def is_in_geofence(area: List[GeoPoint], point: GeoPoint) -> bool:
    """
    Checks if point is inside or outside of geofence
    """
    temp_points: List[tuple[float, float]] = []
    for temp_point in area:
        temp_points.append((temp_point.lat, temp_point.lon))

    lons_lats_vect = np.asarray(temp_points)
    polygon = Polygon(lons_lats_vect)
    point = Point(point.lat, point.lon)
    return bool(polygon.contains(point))


def already_exists(
    points: List[GeoPoint], point: GeoPoint, epsilon_meters: float = 3
) -> bool:
    """
    Checks if a GeoPoint already exists in a list of GeoPoints,
    withing an error margin of `epsilon_meters`.
    """
    for item in points:
        distance_meters = distance.distance(
            (point.lat, point.lon), (item.lat, item.lon)
        ).m
        if distance_meters < epsilon_meters:
            return True
    return False


def shift_of_coordinate(
    point: GeoPoint, north_shift: float, east_shift: float
) -> GeoPoint:
    """
    Calcs the coordinates of a shifted point
    """
    lat = north_to_lat(north_shift, point.lat)
    lon = east_to_lon(east_shift, point.lat, point.lon)
    geopoint = GeoPoint(lat, lon, 0)
    return geopoint


class WaypointsCreator:
    def __init__(
        self,
        altitude,
        points=None,
        geojson_path="./area.json",
        save_debug_map=False,
        save_json=False,
    ) -> None:
        self.rectangles: List[List[GeoPoint]] = []
        self.rectangle_mid_points: List[GeoPoint] = []
        self.geojson_path = geojson_path
        self.altitude = altitude
        self.area = self.open_geojson(points)
        self.real_width, self.real_height = self.get_real_dimensions()
        self.save_debug_map = save_debug_map
        self.save_json = save_json

    def open_geojson(self, points: List[GeoPoint]) -> List[GeoPoint]:
        """
        Loads the json file.

        If the first and last point are identical, it deletes the last one.
        """
        if points is not None:
            return points

        with open(self.geojson_path, "r") as f:
            polygon = json.load(f)
        polygon = polygon["features"][0]["geometry"]["coordinates"][0]
        if polygon[0] == polygon[len(polygon) - 1]:
            polygon = polygon[:-1]

        area = []
        for point in polygon:
            lon, lat = point
            geopoint = GeoPoint(lat, lon, 0)
            area.append(geopoint)
        return area

    def get_real_dimensions(self):
        """
        real dimensions of area to be photographed
        """
        camera = Camera(
            focal_lenght=config.FOCAL,
            sensor_width=config.SENSOR_W,
            sensor_height=config.SENSOR_H,
            res_x=config.RES_X,
            res_y=config.RES_Y,
        )

        width = camera.get_real_width(self.altitude)
        height = camera.get_real_height(self.altitude)

        return width - (0.1 * width), height - (0.1 * height)

    def get_rectangle(self, mid_point: GeoPoint) -> List[GeoPoint]:
        """
        Calcs the geographic coordinates of the rectangle.
        returns a list of the rectangle vertices
        """
        rectangle: List[GeoPoint] = []
        rectangle.append(
            shift_of_coordinate(mid_point, -self.real_height / 2, -self.real_width / 2)
        )
        rectangle.append(
            shift_of_coordinate(mid_point, -self.real_height / 2, self.real_width / 2)
        )
        rectangle.append(
            shift_of_coordinate(mid_point, self.real_height / 2, self.real_width / 2)
        )
        rectangle.append(
            shift_of_coordinate(mid_point, self.real_height / 2, -self.real_width / 2)
        )
        return rectangle

    def process_new_rectangles(self, points: List[GeoPoint]):
        """
        If new rectangle does not already exist, it adds an item to the list
        and looking for new rectangles.
        """
        for point in points:
            if already_exists(self.rectangle_mid_points, point) is False:
                new_rectangle = self.get_rectangle(point)
                self.rectangle_mid_points.append(point)
                self.rectangles.append(new_rectangle)
                return self.divide_area(new_rectangle)
        return False

    def divide_area(self, rectangle: List[GeoPoint]):
        """
        Recursively divides the area into rectangles.
        """
        mid_of_rectangle = find_mid_point(rectangle)
        for vertex in rectangle:
            if is_in_geofence(self.area, vertex):
                vector = GeoPoint(
                    vertex.lat - mid_of_rectangle.lat,
                    vertex.lon - mid_of_rectangle.lon,
                    0,
                )
                vector_lat_sign = vector.lat / abs(vector.lat)
                vector_lon_sign = vector.lon / abs(vector.lon)

                new_mid_point1 = shift_of_coordinate(
                    mid_of_rectangle,
                    vector_lat_sign * self.real_height,
                    vector_lon_sign * self.real_width,
                )
                new_mid_point2 = shift_of_coordinate(
                    mid_of_rectangle, vector_lat_sign * self.real_height, 0
                )
                new_mid_point3 = shift_of_coordinate(
                    mid_of_rectangle, 0, vector_lon_sign * self.real_width
                )
                self.process_new_rectangles(
                    [new_mid_point1, new_mid_point2, new_mid_point3]
                )

    def add_altitude(self):
        for point in self.rectangle_mid_points:
            point.altitude = self.altitude

    def run(self) -> List[GeoPoint]:
        mid_point = find_mid_point(self.area)
        self.process_new_rectangles([mid_point])
        self.add_altitude()

        if self.save_json:
            list_to_json.save_json(self.rectangle_mid_points, "points.json")
        if self.save_debug_map:
            import drone.nav.debug_map as debug_map

            debug_map.save_map(self.rectangle_mid_points, self.rectangles, self.area)

        print("real size of rectangle:", self.real_width, self.real_height)
        print("num of points:", len(self.rectangle_mid_points))
        print("The points have been saved in the points.json file")

        return self.rectangle_mid_points


# if __name__ == "__main__":
#     w = WaypointsCreator(15, save_json=True, save_debug_map=True)
#     w.run()
