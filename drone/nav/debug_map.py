from typing import List

import folium

from drone.nav.position_gps import PositionGPS


def add_markers(points, m):
    for point in points:
        folium.Marker(
            location=(point.lat, point.lon), popup=(point.lat, point.lon)
        ).add_to(m)
    return m


def draw_lines(rectangles: List[List[PositionGPS]], m, line_color):

    for rectangle in rectangles:
        rectangle.append(rectangle[0])
        temp_list = list()
        for item in rectangle:
            temp_list.append((item.lat, item.lon))
        folium.PolyLine(temp_list, color=line_color, weight=2, opacity=0.5).add_to(m)
    return m


def draw_area(area, m, line_color):
    area.append(area[0])
    temp_list = list()
    for item in area:
        temp_list.append((item.lat, item.lon))
    folium.PolyLine(temp_list, color=line_color, weight=5, opacity=0.3).add_to(m)
    return m


def save_map(points_list, rectangles, area):
    # GENERATE MAP

    center = [points_list[0].lat, points_list[0].lon]
    m = folium.Map(location=center, zoom_start=20)

    m = draw_area(area, m, "red")
    # m = add_markers(points_list, m)
    m = draw_lines(rectangles, m, "blue")

    m.save("./index.html")
