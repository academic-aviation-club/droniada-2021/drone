from dataclasses import dataclass

from geopy import distance


@dataclass
class PositionGPS:
    lat: float
    lon: float
    altitude: float


@dataclass
class PositionNED:
    north: float
    east: float
    down: float


@dataclass
class Attitude:
    pitch: float
    yaw: float
    roll: float
    heading: float


def distance_between_gps_points(p1: PositionGPS, p2: PositionGPS):
    return distance.distance([p1.lat, p1.lon], [p2.lat, p2.lon]).meters + abs(
        p1.altitude - p2.altitude
    )
